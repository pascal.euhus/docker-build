FROM python:3.9.1-buster

COPY files/smallfile.txt /tmp/files/

RUN pip install pip --upgrade && \
    pip install \
        ansible \
        awscli

# Another intensive workload (eg. building bin from source)
RUN sleep 30

ENTRYPOINT ["/bin/bash"]
